package util

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"

	"github.com/klauspost/cpuid/v2"
)

func GetHostname() string {
	hostname, err := os.Hostname()

	if err != nil {
		fmt.Println(err)
	}

	return string(hostname)
}

func GetUsername() string {
	username, err := user.Current()

	if err != nil {
		fmt.Println(err)
	}

	return string(username.Username)
}

func GetDesktopEnvironment() string {
	de := os.Getenv("XDG_CURRENT_DESKTOP")

	if de == "" {
		return "None"
	}

	return de
}

func GetCPU() (string, string) {
	cpu_name := cpuid.CPU.BrandName
	cpu_name = strings.Replace(cpu_name, "(R)", "", -1)
	cpu_name = strings.Replace(cpu_name, "(TM)", "", -1)

	var color string = "\033[0m"

	switch cpuid.CPU.VendorString {
	case "GenuineIntel":
		color = "\033[34m"
	case "AuthenticAMD":
		color = "\033[31m"
	}

	return fmt.Sprint(cpu_name), color
}

func GetDistro() string {
	file, err := os.Open("/etc/os-release")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var distro string

	for scanner.Scan() {
		text := scanner.Text()

		if strings.HasPrefix(text, "PRETTY_NAME=") {
			distro = strings.Trim(text[12:], `'"`)
			break
		}
	}

	return distro
}

func GetPackageCount(distro string) string {
	var cmd []string
	var postfix string

	switch strings.Split(strings.ToLower(distro), " ")[0] {
	case "debian":
		cmd = strings.Split("/usr/bin/dpkg --list", " ")
		postfix = "dpkg"
	case "fedora", "opensuse":
		cmd = strings.Split("/usr/bin/rpm -qa", " ")
		postfix = "rpm"
	case "arch":
		cmd = strings.Split("/usr/bin/pacman -Q", " ")
		postfix = "pacman"
	default:
		return "unknown"
	}

	command := exec.Command(cmd[0])
	for i := 1; i < len(cmd); i++ {
		command.Args = append(command.Args, cmd[i])
	}

	output, err := command.Output()
	if err != nil {
		fmt.Println(err)
	}

	pkg_count := len(strings.Split(string(output), "\n")) - 1
	return fmt.Sprintf("%d (%s)", pkg_count, postfix)
}

func GetUptime() string {
	file, err := os.Open("/proc/uptime")
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var uptime string

	for scanner.Scan() {
		text := strings.Split(scanner.Text(), " ")[0]
		uptime_secs_all, err := strconv.ParseFloat(text, 64)
		if err != nil {
			fmt.Println(err)
		}

		uptime_hrs := int(uptime_secs_all / 3600)
		uptime_mins := int(uptime_secs_all/60) - (uptime_hrs * 60)

		uptime = fmt.Sprintf("%d hours %d minutes", uptime_hrs, uptime_mins)
	}

	return uptime
}
